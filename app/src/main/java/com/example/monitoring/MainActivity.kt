package com.example.monitoring

import android.os.Bundle
import android.view.View
import android.widget.RadioButton
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.google.android.material.bottomnavigation.BottomNavigationView


class MainActivity : AppCompatActivity() {

    private lateinit var bottomNavigationView: BottomNavigationView


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //initialisation
        bottomNavigationView = findViewById(R.id.bottomNavigationView)

        //creation des instances de chaque fragment
        val acceuilFragment=AcceuilFragment()
        val apiFragment=APIFragment()
        val profilFragment=ProfilFragment()
        val requestFragment = RequestFragment()

        //Initialisation du premier fragment
        setCurrentFragment(acceuilFragment)

        //attendre le click sur un item de la barre de navigation
        //deplacement d'un fragment a un autre
        bottomNavigationView.setOnItemSelectedListener {
            when(it.itemId){
                R.id.acceuilFragment->setCurrentFragment(acceuilFragment)
                R.id.requestFragment->setCurrentFragment(requestFragment)
                R.id.APIFragment->setCurrentFragment(apiFragment)
                R.id.profilFragment->setCurrentFragment(profilFragment)
            }
            true
        }


    }
    //fonction permettant de pointer vers un fragment
    private fun setCurrentFragment(fragment: Fragment)=
        supportFragmentManager.beginTransaction().apply {
            replace(R.id.fragmentContainerView,fragment)
            commit()
        }
}
