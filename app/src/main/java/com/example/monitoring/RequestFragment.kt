package com.example.monitoring

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.RadioButton
import android.widget.RadioGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class RequestFragment : Fragment() {
    private lateinit var radioGroup: RadioGroup
    private lateinit var radioButton: RadioButton
    private lateinit var imageButton: ImageButton
    private lateinit var  recyclerview : RecyclerView
    private lateinit var textView: TextView
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_request, container, false)

        //variable initialization
        radioGroup = view.findViewById(R.id.IdRequestTypeGroup)
        imageButton = view.findViewById(R.id.IdAddNewHeadButton)
        textView = view.findViewById(R.id.IdAddNewHead)
        recyclerview = view.findViewById(R.id.recycleView)
        val data = ArrayList<ItemsViewModel>()
        val adapter = MyCostomAdapter(data)


        //get radio button id and set et listener on it
        radioGroup.setOnCheckedChangeListener { _, checkedID ->
            radioButton = view.findViewById(checkedID)
            radioButton.setOnClickListener { onRadioButtonClicked(radioButton)
            }
        }

        data.add(ItemsViewModel("Header name", "Header value"))
        // This will pass the ArrayList to our Adapter
        // Setting the Adapter with the recyclerview
        recyclerview.adapter = adapter
        // this creates a vertical layout Manager
        recyclerview.layoutManager = LinearLayoutManager(activity)
        //set a listener on image view for adding new header name and value

        imageButton.setOnClickListener{
            data.add(ItemsViewModel("Header name", "Header value"))
            adapter.notifyDataSetChanged()
        }

        return view
    }
    private fun onRadioButtonClicked(rad : RadioButton) {
        // Check which radio button was clicked
        when(rad.text.toString()) {
            "GET" -> {
                //set header name and value visibility gone
                recyclerview.visibility = View.GONE
                imageButton.visibility = View.GONE
                textView.visibility = View.GONE
            }
            "POST" -> {
                //set header name and value visibility true
                recyclerview.visibility = View.VISIBLE
                imageButton.visibility = View.VISIBLE
                textView.visibility = View.VISIBLE
            }
            "DELETE" -> {
                //set header name and value visibility true
                recyclerview.visibility = View.VISIBLE
                imageButton.visibility = View.VISIBLE
                textView.visibility = View.VISIBLE
            }
        }
    }

}