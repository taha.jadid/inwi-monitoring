package com.example.monitoring

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.textfield.TextInputLayout

class MyCostomAdapter(private val mList: ArrayList<ItemsViewModel>) : RecyclerView.Adapter<MyCostomAdapter.ViewHolder>() {

    // create new views
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        // inflates the card_view_design view
        // that is used to hold list item
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.head_nv_design, parent, false)

        return ViewHolder(view)
    }

    // binds the list items to a view
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val itemViewModel = mList[position]

        // sets the head name to the TextInputEditText from our itemHolder class
        holder.headNameView.hint = itemViewModel.head_name

        // sets the head value to the TextInputEditText from our itemHolder class
        holder.headValueView.hint = itemViewModel.head_value

    }

    // return the number of the items in the list
    override fun getItemCount(): Int {
        return mList.size
    }

    // Holds the views for adding it to image and text
    class ViewHolder(ItemView: View) : RecyclerView.ViewHolder(ItemView) {
        val headNameView: TextInputLayout = itemView.findViewById(R.id.IdHeadView)
        val headValueView: TextInputLayout = itemView.findViewById(R.id.IdHeadValueView)
    }
}
